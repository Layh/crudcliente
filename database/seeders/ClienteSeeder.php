<?php

namespace Database\Seeders;

use App\Models\Cliente;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Cliente::create(
            [
                'db_name' => '12345678910',
                'razao_social' => 'Maria',
                'cnpj' => '12345678910',
                'tipo_base' => 'sp',
                'email' => 'v@gmail.com',
             
                
              
            ]
        );
        Cliente::create(
            [
                'db_name' => '10987654321',
                'razao_social' => 'Lucas',
                'cnpj' => '10987654321',
                'tipo_base' => 'sp',
                'email' => 'v@gmail.com',
               
            ]
        );
    }
}