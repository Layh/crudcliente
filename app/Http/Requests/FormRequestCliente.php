<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormRequestCliente extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        $request = [];
        if ($this->method() == "POST" ) {
            $request = [
                'db_name' => 'required',
                'razao_social' => 'required',
                'documento' => 'required',
                'tipo_base' => 'required',
               
            ];
        }
        if ( $this->method() == "PUT") {
            $request = [
                'razao_social' => 'required',
                'documento' => 'required',
                'tipo_base' => 'required',
            ];
        }
        
        return $request;
    }
}