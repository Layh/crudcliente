<?php

namespace App\Http\Controllers;

use App\Http\Requests\FormRequestCliente;
use App\Models\Cliente;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class ClienteController extends Controller
{
    private $cliente;
    
    public function __construct(Cliente $cliente)
    {
        $this->cliente = $cliente;
    }

    public function index(Request $request)
    {
        $pesquisar = $request->pesquisar;
        $findCliente = $this->cliente->getClientesPesquisarIndex(search: $pesquisar ?? '');

        return view('pages.clientes.paginacao', compact('findCliente'));
    }

    public function deleteCliente(Request $request)
    {

        $db_name = $request->input('db_name');
    
        if (!$db_name) {
            return response()->json(['error' => 'Parâmetro "db_name" não fornecido'], 400);
        }
    
        $cliente = Cliente::where('db_name', $db_name)->first();
    
        if (!$cliente) {
            return response()->json(['error' => 'Cliente não encontrado'], 404);
        }
    
     
        try {
            $cliente->delete();
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            Log::error('Erro ao excluir cliente: ' . $e->getMessage());
            return response()->json(['error' => 'Erro ao excluir cliente'], 500);
        }
    }
 
    public function cadastrarCliente(FormRequestCliente $request)
    {
        if ($request->method() == "POST") {
            $data = $request->all();
            Cliente::create($data);

            Toastr::success('Dados gravados com sucesso.');
            return redirect()->route('clientes.index');
        }
        // mostrar os dados
        return view('pages.clientes.create');
    }

    public function atualizarCliente(FormRequestCliente $request, $db_name)
    {
        if ($request->method() == "PUT") {
            $data = $request->all();
            $buscaRegistro = Cliente::find($db_name);
            $buscaRegistro->update($data);

            Toastr::success('Dados atualizados com sucesso.');
            return redirect()->route('clientes.index');
        }
        $findCliente = Cliente::where('db_name', '=', $db_name)->first();

        return view('pages.clientes.atualiza', compact('findCliente'));
    }
}