<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Produto;
use App\Models\User;
use App\Models\Venda;
use Illuminate\Http\Request;

class DashbordController extends Controller
{
    public function index()
    {
        $totalDeClienteCadastrado = $this->buscaTotalClienteCadastrado();
        $totalDeUsuarioCadastrado = $this->buscaTotalUsuarioCadastrado();


        return view('pages.dashboard.dashboard', compact( 'totalDeClienteCadastrado', 'totalDeUsuarioCadastrado'));
    }


    public function buscaTotalClienteCadastrado()
    {
        $find = Cliente::all()->count();

        return $find;
    }

    public function buscaTotalUsuarioCadastrado()
    {
        $find = User::all()->count();

        return $find;
    }
}