<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;
    
    protected $primaryKey = 'db_name';

    protected $fillable = [
        'db_name',
        'razao_social',
        'documento',
        'tipo_base',
        'nome',
        'email',
    ];

    public function getClientesPesquisarIndex(string $search = '')
    {
        $clientes = $this->where(function ($query) use ($search) {
            if ($search) {
                $query->where('razao_social', $search);
                $query->orWhere('razao_social', 'LIKE', "%{$search}%");
            }
        })->get();

        return $clientes;
    }
}