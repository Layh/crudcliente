{{-- Extendendo da Index --}}
@extends('index')

@section('content')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Clientes</h1>
    </div>
    <div>
        <form action="{{ route('clientes.index') }}" method="get">
            <input type="text" name="pesquisar" placeholder="Digite o nome" />
            <button> Pesquisar </button>
            <a type="button" href="{{ route('cadastrar.cliente') }}" class="btn btn-success float-end">
                Incluir Cliente
            </a>
        </form>
        <div class="table-responsive mt-4">
            @if ($findCliente->isEmpty())
                <p> Não existe dados </p>
            @else
                <table class="table table-striped table-sm">
                    <thead>
                        <tr>
                            <th>Nome da Base</th>
                            <th>Razão Social</th>
                            <th>CNPJ/CPF</th>
                            <th>Tipo de Base</th>
                            <th>E-mail</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($findCliente as $cliente)
                            <tr>
                                <td>{{ $cliente->db_name }}</td>
                                <td>{{ $cliente->razao_social }}</td>
                                <td>{{ $cliente->documento}}</td>
                                <td>{{ $cliente->tipo_base}}
                                <td>{{ $cliente->email }}
                              
                    
                                <td>
                                    <a href="{{ route('atualizar.cliente', $cliente->db_name) }}" class="btn btn-light btn-sm">
                                        Editar
                                    </a>

                                    <meta name='csrf-token' content=" {{ csrf_token() }}" />
                                
                                    <a onclick="deleteRegistroPaginacaoCliente('{{ route('cliente.delete') }}', '{{ $cliente->db_name }}')"
                                        class="btn btn-danger btn-sm">
                                         Excluir
                                     </a>
                                    
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>

    </div>
@endsection
