@extends('index')

@section('content')
    <form class="form" method="POST" action="{{ route('atualizar.cliente', $findCliente->db_name) }}">
        @csrf
        @method('PUT')
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Editar Cliente</h1>
        </div>
        <div class="mb-3">
            <label class="form-label">Nome da Base</label>
            <input type="text" disabled value=" {{ $findCliente->db_name }}"
                class="form-control @error('db_name') is-invalid @enderror" name="db_name">
            @if ($errors->has('db_name'))
                <div class="invalid-feedback"> {{ $errors->first('db_name') }}</div>
            @endif
        </div>
       
        <div class="mb-3">
            <label class="form-label">Razão Social</label>
            <input value=" {{ isset($findCliente->razao_social) ? $findCliente->razao_social : old('razao_social') }}"
                class="form-control @error('razao_social') is-invalid @enderror" name="razao_social">
            @if ($errors->has('razao_social'))
                <div class="invalid-feedback"> {{ $errors->first('razao_social') }}</div>
            @endif
        </div>
    
        <div class="mb-3">
            <label class="form-label">CNPJ/CPF</label>
            <input value=" {{ isset($findCliente->documento) ? $findCliente->documento : old('documento') }}"
                class="form-control @error('documento') is-invalid @enderror" name="documento">
            @if ($errors->has('documento'))
                <div class="invalid-feedback"> {{ $errors->first('cnpj') }}</div>
            @endif
        </div>
       
        <div class="mb-3">
            <label class="form-label">Tipo Base</label>
            <input value=" {{ isset($findCliente->tipo_base) ? $findCliente->tipo_base : old('tipo_base') }}"
                class="form-control @error('tipo_base') is-invalid @enderror" name="tipo_base">
            @if ($errors->has('tipo_base'))
                <div class="invalid-feedback"> {{ $errors->first('tipo_base') }}</div>
            @endif
        </div>
      
        <div class="mb-3">
            <label class="form-label">E-mail</label>
            <input value=" {{ isset($findCliente->email) ? $findCliente->email : old('email') }}"
                class="form-control @error('email') is-invalid @enderror" name="email">
            @if ($errors->has('email'))
                <div class="invalid-feedback"> {{ $errors->first('email') }}</div>
            @endif
        </div>

    

        <button type="submit" class="btn btn-success">GRAVAR</button>
    </form>
@endsection

