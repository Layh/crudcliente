<?php

use App\Http\Controllers\ClienteController;
use App\Http\Controllers\DashbordController;
use App\Http\Controllers\ProdutosController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\VendaController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::prefix('dashboard')->group(function () {
    Route::get('/', [DashbordController::class, 'index'])->name('dashboard.index');
});


Route::prefix('clientes')->group(function () {
    Route::get('/', [ClienteController::class, 'index'])->name('clientes.index');
    //Cadastro Create
    Route::get('/cadastrarCliente', [ClienteController::class, 'cadastrarCliente'])->name('cadastrar.cliente');
    Route::post('/cadastrarCliente', [ClienteController::class, 'cadastrarCliente'])->name('cadastrar.cliente');
    //Atualiza Update
    Route::get('/atualizarCliente/{db_name}', [ClienteController::class, 'atualizarCliente'])->name('atualizar.cliente');
    Route::put('/atualizarCliente/{db_name}', [ClienteController::class, 'atualizarCliente'])->name('atualizar.cliente');
    Route::delete('/delete', [ClienteController::class, 'deleteCliente'])->name('cliente.delete');
  
});


Route::prefix('usuario')->group(function () {
    Route::get('/', [UsuarioController::class, 'index'])->name('usuario.index');
    Route::get('/cadastrarUsuario', [UsuarioController::class, 'cadastrarUsuario'])->name('cadastrar.usuario');
    Route::post('/cadastrarUsuario', [UsuarioController::class, 'cadastrarUsuario'])->name('cadastrar.usuario');
    //Atualiza Update
    Route::get('/atualizarUsuario/{id}', [UsuarioController::class, 'atualizarUsuario'])->name('atualizar.usuario');
    Route::put('/atualizarUsuario/{id}', [UsuarioController::class, 'atualizarUsuario'])->name('atualizar.usuario');
    Route::delete('/delete', [UsuarioController::class, 'delete'])->name('usuario.delete');
});


require __DIR__ . '/auth.php';